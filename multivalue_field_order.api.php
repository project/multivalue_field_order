<?php

/**
 * @file
 * Hooks list.
 */

/**
 * Options to be displayed ad field settings form.
 */
function my_module_custom_multivalue_field_options_alter(&$options) {
  $options = [
    'xxx' => 'yyy',
  ];
}

/**
 * Calling sort function for custom field type.
 */
function my_module_custom_multivalue_field_sort_alter(&$array, $field_type, $parsed_field_order) {
  switch ($field_type) {
    case 'aaa':
      uasort($array, 'compare_callback_' . $parsed_field_order['by']);
      break;
  }
}

/**
 * Compare callback example - ascesnding.
 */
function compare_callback_asc($a, $b) {
  return strcmp($b['value'], $a['value']);
}

/**
 * Compare callback example - descesnding.
 */
function compare_callback_desc($a, $b) {
  return strcmp($b['value'], $a['value']);
}
